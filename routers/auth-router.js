const express = require('express')
const md5 = require('md5')
const router = express.Router()
const db = require('../firebase').firestore

router.get('/', (req, res) => {
    res.json({
        data: 'u_share server: Authentication Service'
    })
})

router.post('/', (req, res) => {
    let username = req.body.username
    let password = req.body.password

    let user = db.collection('users').doc(username).get()
        .then(doc => {
            if (!doc.exists) {
                // User không tồn tại
                res.json({
                    username: username,
                    status: 'user_not_found',
                })
            } else {
                // User tồn tại
                let password_hashed = md5(password)
                if (password_hashed === doc.data().password) {
                    res.json({
                        username: username,
                        status: 'success',
                        data: doc.data()
                    })
                } else {
                    res.json({
                        username: username,
                        status: 'failed'
                    })
                }
            }
        })
        .catch(err => {
            res.json({
                username: username,
                status: 'error',
                data: err
            })
        })
})

module.exports = router