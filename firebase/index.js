const firebase = require('firebase')

const firebaseConfig = {
    apiKey: "AIzaSyB4v2Fc-GxehiGCG8tCOVnbtn3WBKiSk2I",
    authDomain: "ushare-5a7b5.firebaseapp.com",
    databaseURL: "https://ushare-5a7b5.firebaseio.com",
    projectId: "ushare-5a7b5",
    storageBucket: "ushare-5a7b5.appspot.com",
    messagingSenderId: "573956070821"
}

firebase.initializeApp(firebaseConfig)

module.exports = {
    firestore: firebase.firestore()
}