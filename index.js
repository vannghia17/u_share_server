const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

const authService = require('./routers/auth-router')

app.use(cors())
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')))

app.use((req, res, next) => {
    console.log('[REQUEST]')
    console.log('url:', req.originalUrl)
    console.log('body:', req.body)
    console.log('\n')
    
    next()
})

app.get('/', (req, res) => {
    res.json({
        data: 'u_share server: Home'
    })
})

app.use('/auth', authService)

const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
    console.log('Server is listening on port', PORT)
})
