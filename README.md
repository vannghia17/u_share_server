# API Description

Cần có
- Hiển thị tất cả groups
- Click vô group nào thì tới trang group đó:
    - Đổi tên
    - Thêm thành viên -> hiển thị hết danh sách sinh viên (chưa tham gia group đó)

## /auth

Request (POST)

````
{
    username: '1512348',
    password: '1512348'
}
````

Response

````
-- Thành công
{
    "username": "1512348",
    "status": "success",
    "data": {
        "department": "CNTT",
        "name": "Nguyễn Văn Nghĩa",
        "password": "60f0170b689d24d36f5b1942d84685a3",
        "sex": "male"
    }
}

-- Sai mật khẩu 
{
    "username": "1512348",
    "status": "failed"
}

-- Người dùng không tồn tại
{
    "username": "151234",
    "status": "user_not_found"
}
````